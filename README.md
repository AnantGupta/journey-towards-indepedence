
# Journey Towards Independence

India's freedom struggle is something we've all grown up hearing about. A British colony for nearly two centuries, India fought long and hard to earn Swaraj (self-rule).
Through this website, we wish to describe to you the story of India's Journey toward freedom and it's unknown heroes


## License

[MIT](https://choosealicense.com/licenses/mit/)


## Website

Link For The Website: https://anantgupta.gitlab.io/journey-towards-indepedence/    
## Screenshots

![Website Screenshot](https://i.imgur.com/ZHAJPs1.png)

## About Us
This website is made possible by a team work by Anant Gupta & Naitik Das aka Anonymous Coders